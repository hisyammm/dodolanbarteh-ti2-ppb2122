package com.dodolanbarteh.pemesanantiket

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_data_pesawat.*
import kotlinx.android.synthetic.main.activity_main.*

class DataPesawatActivit : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_pesawat)
        toolbar.setOnClickListener { v: View? ->
            val intent = Intent(this@DataPesawatActivit, MainActivity::class.java)
            startActivity(intent)
        }
    }
}